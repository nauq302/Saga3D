// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_SCENE_MANAGER_H_INCLUDED__
#define __C_SCENE_MANAGER_H_INCLUDED__

#include "IMesh.h"
#include "ISceneManager.h"
#include <unordered_map>

namespace saga
{
namespace scene
{
  class ISceneNode;

  /*!
    The Scene Manager manages scene nodes, mesh resources, cameras and all the other stuff.
  */
  class CSceneManager : public ISceneManager, public std::enable_shared_from_this<CSceneManager>
  {
  public:
    //! constructor
    CSceneManager(const std::shared_ptr<video::IVideoDriver>& driver);

    //! destructor
    virtual ~CSceneManager();

    virtual void resetID() { MeshID = TextureID = NodeID = INVALID_ID; }
    virtual ID getMeshID() override { return ++MeshID; }
    virtual ID getTextureID() override { return ++TextureID; }
    virtual ID getNodeID() override { return ++NodeID; }

    virtual void onEvent(const SDL_Event& event) override;

    virtual const std::shared_ptr<IMesh>& getMesh(const std::string& fileName) override;

    virtual const std::shared_ptr<IMesh>& getMesh(void* data, const std::size_t size,
      const std::string& extension) override;

    virtual const std::shared_ptr<IMesh>& getMesh(const ID id) const override
    { return MeshCache.at(id); }

    virtual void removeMesh(const std::shared_ptr<IMesh>& mesh) override
    { MeshCache.erase(mesh->getID()); }

    virtual void removeMesh(const ID id) override
    { MeshCache.erase(id); }

    //! adds a scene node for rendering a static mesh
    //! the returned pointer must not be dropped.
    virtual std::shared_ptr<ISceneNode> createSceneNode(
      const std::shared_ptr<IMesh>& mesh, const std::shared_ptr<ISceneNode>& parent = nullptr,
      const glm::vec3& position = glm::vec3(0,0,0),
      const glm::vec3& rotation = glm::vec3(0,0,0),
      const glm::vec3& scale = glm::vec3(1.0f, 1.0f, 1.0f)) override;

    virtual void removeNode(const ID id)
    { NodeCache.erase(id); }

    virtual void removeNode(const std::shared_ptr<ISceneNode>& node) override
    { removeNode(node->getID()); }

    // //! gets an animateable mesh. loads it if needed. returned pointer must not be dropped.
    // virtual IAnimatedMesh* getMesh(const std::string& filename, const std::string& alternativeCacheName) override;

    // //! gets an animateable mesh. loads it if needed. returned pointer must not be dropped.
    // virtual IAnimatedMesh* getMesh(io::IReadFile* file) override;

    //! returns the video driver
    virtual const std::shared_ptr<video::IVideoDriver>& getVideoDriver() const override { return Driver; }

    virtual void registerNode(const std::shared_ptr<ISceneNode>& node, video::RenderPassHandle pass) override;

    virtual const std::shared_ptr<ISceneNode>& getNode(const ID id) const override
    {
      return NodeCache.at(id);
    }

    virtual void unregisterNode(const std::shared_ptr<ISceneNode>& node, video::RenderPassHandle pass) override;

    virtual bool isEmptyPass(video::RenderPassHandle pass) const override { return NodeList.empty() || NodeList.at(pass).empty(); }

    virtual SceneNodeList& getNodeList(const video::RenderPassHandle pass) override { return NodeList[pass]; }

    virtual PassSceneNodeList& getNodeList() override { return NodeList; }

    virtual void clearRegisteredNodes() override { NodeList.clear(); }

  //   //! adds Volume Lighting Scene Node.
  //   //! the returned pointer must not be dropped.
  //   virtual IVolumeLightSceneNode* addVolumeLightSceneNode(ISceneNode* parent= 0, std::int32_t id=-1,
  //     const std::uint32_t subdivU = 32, const std::uint32_t subdivV = 32,
  //     const video::SColor foot = video::SColor(51, 0, 230, 180),
  //     const video::SColor tail = video::SColor(0, 0, 0, 0),
  //     const glm::vec3& position = glm::vec3(0,0,0),
  //     const glm::vec3& rotation = glm::vec3(0,0,0),
  //     const glm::vec3& scale = glm::vec3(1.0f, 1.0f, 1.0f)) override;

  //   //! adds a cube scene node to the scene. It is a simple cube of (1,1,1) size.
  //   //! the returned pointer must not be dropped.
  //   virtual IMeshSceneNode* addCubeSceneNode(float size=10.0f, ISceneNode* parent= 0, std::int32_t id=-1,
  //     const glm::vec3& position = glm::vec3(0,0,0),
  //     const glm::vec3& rotation = glm::vec3(0,0,0),
  //     const glm::vec3& scale = glm::vec3(1.0f, 1.0f, 1.0f)) override;

  //   //! Adds a sphere scene node to the scene.
  //   virtual IMeshSceneNode* addSphereSceneNode(float radius=5.0f, std::int32_t polyCount=16, ISceneNode* parent= 0, std::int32_t id=-1,
  //     const glm::vec3& position = glm::vec3(0,0,0),
  //     const glm::vec3& rotation = glm::vec3(0,0,0),
  //     const glm::vec3& scale = glm::vec3(1.0f, 1.0f, 1.0f)) override;

  //   //! adds a scene node for rendering an animated mesh model
  //   virtual IAnimatedMeshSceneNode* addAnimatedMeshSceneNode(IAnimatedMesh* mesh, ISceneNode* parent= 0, std::int32_t id=-1,
  //     const glm::vec3& position = glm::vec3(0,0,0),
  //     const glm::vec3& rotation = glm::vec3(0,0,0),
  //     const glm::vec3& scale = glm::vec3(1.0f, 1.0f, 1.0f),
  //     bool alsoAddIfMeshPointerZero=false) override;

  //   //! Adds a scene node for rendering a animated water surface mesh.
  //   virtual ISceneNode* addWaterSurfaceSceneNode(IMesh* mesh, float waveHeight, float waveSpeed, float wlength, ISceneNode* parent= 0, std::int32_t id=-1,
  //     const glm::vec3& position = glm::vec3(0,0,0),
  //     const glm::vec3& rotation = glm::vec3(0,0,0),
  //     const glm::vec3& scale = glm::vec3(1.0f, 1.0f, 1.0f)) override;

  //   //! renders the node.
  //   virtual void render() override;

  //   //! returns the axis aligned bounding box of this node
  //   virtual const core::aabbox3d<float>& getBoundingBox() const override;

  //   //! registers a node for rendering it at a specific time.
  //   virtual std::uint32_t registerNodeForRendering(ISceneNode* node, E_SCENE_NODE_RENDER_PASS pass = E_SCENE_NODE_RENDER_PASS::AUTOMATIC) override;


  //! animate all scene nodes
  virtual void animate(const video::RenderPassHandle pass = video::NULL_GPU_RESOURCE_HANDLE) override;

  //   //! Adds a scene node for rendering using a octree to the scene graph. This a good method for rendering
  //   //! scenes with lots of geometry. The Octree is built on the fly from the mesh, much
  //   //! faster then a bsp tree.
  //   virtual IOctreeSceneNode* addOctreeSceneNode(IAnimatedMesh* mesh, ISceneNode* parent= 0,
  //     std::int32_t id=-1, std::int32_t minimalPolysPerNode=512, bool alsoAddIfMeshPointerZero=false) override;

  //   //! Adss a scene node for rendering using a octree. This a good method for rendering
  //   //! scenes with lots of geometry. The Octree is built on the fly from the mesh, much
  //   //! faster then a bsp tree.
  //   virtual IOctreeSceneNode* addOctreeSceneNode(IMesh* mesh, ISceneNode* parent= 0,
  //     std::int32_t id=-1, std::int32_t minimalPolysPerNode=128, bool alsoAddIfMeshPointerZero=false) override;

    //! Adds a camera scene node to the tree and sets it as active camera.
    //! \param position: Position of the space relative to its parent where the camera will be placed.
    //! \param lookAt: Position where the camera will look at. Also known as target.
    //! \param parent: Parent scene node of the camera. Can be null. If the parent moves,
    //! the camera will move too.
    //! \return Pointer to interface to camera
    virtual const std::shared_ptr<ICameraSceneNode>& addCameraSceneNode(
      const std::shared_ptr<ISceneNode>& parent = nullptr,
      const glm::vec3& position = glm::vec3(0,0,0),
      const glm::vec3& lookAt = glm::vec3(0,0,-1),
      bool makeActive = true) override;

  //   //! Adds a camera scene node which is able to be controlle with the mouse similar
  //   //! like in the 3D Software Maya by Alias Wavefront.
  //   //! The returned pointer must not be dropped.
  //   virtual ICameraSceneNode* addCameraSceneNodeMaya(ISceneNode* parent= 0,
  //     float rotateSpeed=-1500.f, float zoomSpeed=200.f,
  //     float translationSpeed=1500.f, std::int32_t id=-1, float distance=70.f,
  //     bool makeActive=true) override;

  //! Adds a camera scene node which is able to be controled with the mouse and keys
  //! like in most first person shooters (FPS):
  virtual const std::shared_ptr<ICameraSceneNode>& addCameraSceneNodeFPS(
    const std::shared_ptr<ISceneNode>& parent = nullptr,
    float moveSpeed = 0.5f, float rotateSpeed = 1.f,
    bool makeActive = true) override;

  //   //! Adds a dynamic light scene node. The light will cast dynamic light on all
  //   //! other scene nodes in the scene, which have the material flag video::MTF_LIGHTING
  //   //! turned on. (This is the default setting in most scene nodes).
  //   virtual ILightSceneNode* addLightSceneNode(ISceneNode* parent = 0,
  //     const glm::vec3& position = glm::vec3(0,0,0),
  //     video::SColorf color = video::SColorf(1.0f, 1.0f, 1.0f),
  //     float range=100.0f, std::int32_t id=-1) override;

  //   //! Adds a billboard scene node to the scene. A billboard is like a 3d sprite: A 2d element,
  //   //! which always looks to the camera. It is usually used for things like explosions, fire,
  //   //! lensflares and things like that.
  //   virtual IBillboardSceneNode* addBillboardSceneNode(ISceneNode* parent = 0,
  //     const glm::vec2& size = glm::vec2(10.0f, 10.0f),
  //     const glm::vec3& position = glm::vec3(0,0,0), std::int32_t id=-1,
  //     video::SColor shadeTop = 0xFFFFFFFF, video::SColor shadeBottom = 0xFFFFFFFF) override;

  //   //! Adds a skybox scene node. A skybox is a big cube with 6 textures on it and
  //   //! is drawn around the camera position.
  //   virtual ISceneNode* addSkyBoxSceneNode(video::ITexture* top, video::ITexture* bottom,
  //     video::ITexture* left, video::ITexture* right, video::ITexture* front,
  //     video::ITexture* back, ISceneNode* parent = 0, std::int32_t id=-1) override;

  //   //! Adds a skydome scene node. A skydome is a large (half-) sphere with a
  //   //! panoramic texture on it and is drawn around the camera position.
  //   virtual ISceneNode* addSkyDomeSceneNode(video::ITexture* texture,
  //     std::uint32_t horiRes=16, std::uint32_t vertRes=8,
  //     float texturePercentage= 0.9, float spherePercentage=2.0,float radius = 1000.f,
  //     ISceneNode* parent= 0, std::int32_t id=-1) override;

  //   //! Adds a text scene node, which is able to display
  //   //! 2d text at a position in three dimensional space
  //   virtual ITextSceneNode* addTextSceneNode(gui::IGUIFont* font, const char* text,
  //     video::SColor color=video::SColor(100,255,255,255),
  //     ISceneNode* parent = 0,  const glm::vec3& position = glm::vec3(0,0,0),
  //     std::int32_t id=-1) override;

  //   //! Adds a text scene node, which uses billboards
  //   virtual IBillboardTextSceneNode* addBillboardTextSceneNode(gui::IGUIFont* font, const char* text,
  //     ISceneNode* parent = 0,
  //     const glm::vec2& size = glm::vec2(10.0f, 10.0f),
  //     const glm::vec3& position = glm::vec3(0,0,0), std::int32_t id=-1,
  //     video::SColor colorTop = 0xFFFFFFFF, video::SColor colorBottom = 0xFFFFFFFF) override;

  //   //! Adds a scene node, which can render a quake3 shader
  //   virtual IMeshSceneNode* addQuake3SceneNode(const IMeshBuffer* meshBuffer, const quake3::IShader * shader,
  //     ISceneNode* parent= 0, std::int32_t id=-1) override;


  //   //! Adds a Hill Plane mesh to the mesh pool. The mesh is
  //   //! generated on the fly and looks like a plane with some hills
  //   //! on it. You can specify how many hills should be on the plane
  //   //! and how high they should be. Also you must specify a name
  //   //! for the mesh because the mesh is added to the mesh pool and
  //   //! can be retrieved back using ISceneManager::getMesh with the
  //   //! name as parameter.
  //   virtual IAnimatedMesh* addHillPlaneMesh(const std::string& name,
  //     const glm::vec2& tileSize, const glm::uvec2& tileCount,
  //     video::SMaterial* material = 0,  float hillHeight = 0.0f,
  //     const glm::vec2& countHills = glm::vec2(1.0f, 1.0f),
  //     const glm::vec2& textureRepeatCount = glm::vec2(1.0f, 1.0f)) override;

  //   //! Adds a terrain mesh to the mesh pool.
  //   virtual IAnimatedMesh* addTerrainMesh(const std::string& meshname,  video::IImage* texture, video::IImage* heightmap,
  //     const glm::vec2& stretchSize = glm::vec2(10.0f,10.0f),
  //     float maxHeight=200.0f,
  //     const glm::uvec2& defaultVertexBlockSize = glm::uvec2(64,64)) override;

  //   //! Add a arrow mesh to the mesh pool
  //   virtual IAnimatedMesh* addArrowMesh(const std::string& name,
  //       video::SColor vtxColor0, video::SColor vtxColor1,
  //       std::uint32_t tesselationCylinder, std::uint32_t tesselationCone,
  //       float height, float cylinderHeight, float width0,
  //       float width1) override;

  //   //! Adds a static sphere mesh to the mesh pool.
  //   virtual IAnimatedMesh* addSphereMesh(const std::string& name,
  //       float radius=5.f, std::uint32_t polyCountX=16, std::uint32_t polyCountY=16) override;

  //   //! Adds a static volume light mesh to the mesh pool.
  //   virtual IAnimatedMesh* addVolumeLightMesh(const std::string& name,
  //     const std::uint32_t SubdivideU = 32, const std::uint32_t SubdivideV = 32,
  //     const video::SColor FootColor = video::SColor(51, 0, 230, 180),
  //     const video::SColor TailColor = video::SColor(0, 0, 0, 0)) override;

  //   //! Adds a particle system scene node.
  //   virtual IParticleSystemSceneNode* addParticleSystemSceneNode(
  //     bool withDefaultEmitter=true, ISceneNode* parent= 0, std::int32_t id=-1,
  //     const glm::vec3& position = glm::vec3(0,0,0),
  //     const glm::vec3& rotation = glm::vec3(0,0,0),
  //     const glm::vec3& scale = glm::vec3(1.0f, 1.0f, 1.0f)) override;

  //   //! Adds a terrain scene node to the scene graph.
  //   virtual ITerrainSceneNode* addTerrainSceneNode(
  //     const std::string& heightMapFileName,
  //     ISceneNode* parent= 0, std::int32_t id=-1,
  //     const glm::vec3& position = glm::vec3(0.0f,0.0f,0.0f),
  //     const glm::vec3& rotation = glm::vec3(0.0f,0.0f,0.0f),
  //     const glm::vec3& scale = glm::vec3(1.0f,1.0f,1.0f),
  //     video::SColor vertexColor = video::SColor(255,255,255,255),
  //     std::int32_t maxLOD=4, E_TERRAIN_PATCH_SIZE patchSize=E_TERRAIN_PATCH_SIZE::SIZE_17,std::int32_t smoothFactor= 0,
  //     bool addAlsoIfHeightmapEmpty = false) override;

  //   //! Adds a terrain scene node to the scene graph.
  //   virtual ITerrainSceneNode* addTerrainSceneNode(
  //     io::IReadFile* heightMap,
  //     ISceneNode* parent= 0, std::int32_t id=-1,
  //     const glm::vec3& position = glm::vec3(0.0f,0.0f,0.0f),
  //     const glm::vec3& rotation = glm::vec3(0.0f,0.0f,0.0f),
  //     const glm::vec3& scale = glm::vec3(1.0f,1.0f,1.0f),
  //     video::SColor vertexColor = video::SColor(255,255,255,255),
  //     std::int32_t maxLOD=4, E_TERRAIN_PATCH_SIZE patchSize=E_TERRAIN_PATCH_SIZE::SIZE_17,std::int32_t smoothFactor= 0,
  //     bool addAlsoIfHeightmapEmpty=false) override;

  //   //! Adds a dummy transformation scene node to the scene graph.
  //   virtual IDummyTransformationSceneNode* addDummyTransformationSceneNode(
  //     ISceneNode* parent= 0, std::int32_t id=-1) override;

  //   //! Adds an empty scene node.
  //   virtual ISceneNode* addEmptySceneNode(ISceneNode* parent, std::int32_t id=-1) override;

  //   //! Returns the root scene node. This is the scene node which is parent
  //   //! of all scene nodes. The root scene node is a special scene node which
  //   //! only exists to manage all scene nodes. It is not rendered and cannot
  //   //! be removed from the scene.
  //   //! \return Pointer to the root scene node.
    virtual const std::shared_ptr<ISceneNode>& getRootSceneNode() const override
    { return RootSceneNode; }

    //! Returns the current active camera.
    //! \return The active camera is returned. Note that this can be NULL, if there
    //! was no camera created yet.
    virtual const std::shared_ptr<ICameraSceneNode>& getActiveCamera() const override { return ActiveCamera; }

    //! Sets the active camera. The previous active camera will be deactivated.
    //! \param camera: The new camera which should be active.
    virtual void setActiveCamera(const std::shared_ptr<ICameraSceneNode>& camera) override;

  //   //! creates a rotation animator, which rotates the attached scene node around itself.
  //   //! \param rotationPerSecond: Specifies the speed of the animation
  //   //! \return The animator. Attach it to a scene node with ISceneNode::addAnimator()
  //   //! and the animator will animate it.
  //   virtual ISceneNodeAnimator* createRotationAnimator(const glm::vec3& rotationPerSecond) override;

  //   //! creates a fly circle animator
  //   /** Lets the attached scene node fly around a center.
  //   \param center Center relative to node origin
  //   \param speed: The orbital speed, in radians per millisecond.
  //   \param direction: Specifies the upvector used for alignment of the mesh.
  //   \param startPosition: The position on the circle where the animator will
  //   begin. Value is in multiples  of a circle, i.e. 0.5 is half way around.
  //   \return The animator. Attach it to a scene node with ISceneNode::addAnimator()
  //   */
  //   virtual ISceneNodeAnimator* createFlyCircleAnimator(
  //       const glm::vec3& center=glm::vec3(0.f, 0.f, 0.f),
  //       float radius=100.f, float speed= 0.001f,
  //       const glm::vec3& direction=glm::vec3(0.f, 1.f, 0.f),
  //       float startPosition = 0.f,
  //       float radiusEllipsoid = 0.f) override;

  //   //! Creates a fly straight animator, which lets the attached scene node
  //   //! fly or move along a line between two points.
  //   virtual ISceneNodeAnimator* createFlyStraightAnimator(const glm::vec3& startPoint,
  //     const glm::vec3& endPoint, std::uint32_t timeForWay, bool loop=false,bool pingpong = false) override;

  //   //! Creates a texture animator, which switches the textures of the target scene
  //   //! node based on a list of textures.
  //   virtual ISceneNodeAnimator* createTextureAnimator(const std::vector<video::ITexture*>& textures,
  //     std::int32_t timePerFrame, bool loop) override;

  //   //! Creates a scene node animator, which deletes the scene node after
  //   //! some time automatically.
  //   virtual ISceneNodeAnimator* createDeleteAnimator(std::uint32_t timeMS) override;


  //   //! Creates a special scene node animator for doing automatic collision detection
  //   //! and response.
  //   virtual ISceneNodeAnimatorCollisionResponse* createCollisionResponseAnimator(
  //     ITriangleSelector* world, ISceneNode* sceneNode,
  //     const glm::vec3& ellipsoidRadius = glm::vec3(30,60,30),
  //     const glm::vec3& gravityPerSecond = glm::vec3(0,-1.0f,0),
  //     const glm::vec3& ellipsoidTranslation = glm::vec3(0,0,0),
  //     float slidingValue = 0.0005f) override;

  //   //! Creates a follow spline animator.
  //   virtual ISceneNodeAnimator* createFollowSplineAnimator(std::int32_t startTime,
  //     const std::vector< glm::vec3 >& points,
  //     float speed, float tightness, bool loop, bool pingpong) override;


  //   //! Creates a simple ITriangleSelector, based on a mesh.
  //   virtual ITriangleSelector* createTriangleSelector(IMesh* mesh, ISceneNode* node, bool separateMeshbuffers) override;

  //   //! Creates a simple ITriangleSelector, based on a meshbuffer.
  //   virtual ITriangleSelector* createTriangleSelector(const IMeshBuffer* meshBuffer, std::uint32_t materialIndex, ISceneNode* node) override;

  //   //! Creates a simple ITriangleSelector, based on an animated mesh scene node.
  //   //! Details of the mesh associated with the node will be extracted internally.
  //   //! Call ITriangleSelector::update() to have the triangle selector updated based
  //   //! on the current frame of the animated mesh scene node.
  //   //! \param: The animated mesh scene node from which to build the selector
  //   virtual ITriangleSelector* createTriangleSelector(IAnimatedMeshSceneNode* node, bool separateMeshbuffers) override;

  //   //! Creates a simple ITriangleSelector, based on a mesh.
  //   virtual ITriangleSelector* createOctreeTriangleSelector(IMesh* mesh,
  //     ISceneNode* node, std::int32_t minimalPolysPerNode) override;

  //   //! Creates a simple ITriangleSelector, based on a meshbuffer.
  //   virtual ITriangleSelector* createOctreeTriangleSelector(IMeshBuffer* meshBuffer, std::uint32_t materialIndex,
  //     ISceneNode* node, std::int32_t minimalPolysPerNode=32) override;

  //   //! Creates a simple dynamic ITriangleSelector, based on a axis aligned bounding box.
  //   virtual ITriangleSelector* createTriangleSelectorFromBoundingBox(
  //     ISceneNode* node) override;

  //   //! Creates a meta triangle selector.
  //   virtual IMetaTriangleSelector* createMetaTriangleSelector() override;

  //   //! Creates a triangle selector which can select triangles from a terrain scene node
  //   //! \param: Pointer to the created terrain scene node
  //   //! \param: Level of detail, 0 for highest detail.
  //   virtual ITriangleSelector* createTerrainTriangleSelector(
  //     ITerrainSceneNode* node, std::int32_t LOD= 0) override;

  //   //! Adds an external mesh loader.
  //   virtual void addExternalMeshLoader(IMeshLoader* externalLoader) override;

  //   //! Returns the number of mesh loaders supported by Irrlicht at this time
  //   virtual std::uint32_t getMeshLoaderCount() const override;

  //   //! Retrieve the given mesh loader
  //   virtual IMeshLoader* getMeshLoader(std::uint32_t index) const override;

  //   //! Adds an external scene loader.
  //   virtual void addExternalSceneLoader(ISceneLoader* externalLoader) override;

  //   //! Returns the number of scene loaders supported by Irrlicht at this time
  //   virtual std::uint32_t getSceneLoaderCount() const override;

  //   //! Retrieve the given scene loader
  //   virtual ISceneLoader* getSceneLoader(std::uint32_t index) const override;

  //   //! Returns a pointer to the scene collision manager.
  //   virtual ISceneCollisionManager* getSceneCollisionManager() override;

  //   //! Returns a pointer to the mesh manipulator.
  //   virtual IMeshManipulator* getMeshManipulator() override;

  //   //! Sets the color of stencil buffers shadows drawn by the scene manager.
  //   virtual void setShadowColor(video::SColor color) override;

  //   //! Returns the current color of shadows.
  //   virtual video::SColor getShadowColor() const override;

  //   //! Adds a scene node to the deletion queue.
  //   virtual void addToDeletionQueue(ISceneNode* node) override;

  //   //! Returns the first scene node with the specified id.
  //   virtual ISceneNode* getSceneNodeFromId(std::int32_t id, ISceneNode* start= 0) override;

  //   //! Returns the first scene node with the specified name.
  //   virtual ISceneNode* getSceneNodeFromName(const char* name, ISceneNode* start= 0) override;

  //   //! Returns the first scene node with the specified type.
  //   virtual ISceneNode* getSceneNodeFromType(scene::E_SCENE_NODE_TYPE type, ISceneNode* start= 0) override;

  //   //! returns scene nodes by type.
  //   virtual void getSceneNodesFromType(E_SCENE_NODE_TYPE type, std::vector<scene::ISceneNode*>& outNodes, ISceneNode* start= 0) override;

  //   //! Posts an input event to the environment. Usually you do not have to
  //   //! use this method, it is used by the internal engine.
  //   virtual bool postEventFromUser(const SEvent& event) override;

  //   //! Clears the whole scene. All scene nodes are removed.
    virtual void clear() override;

  //   //! Removes all children of this scene node
  //   virtual void removeAll() override;

  //   //! Returns interface to the parameters set in this scene.
  //   virtual io::IAttributes* getParameters() override;

  //   //! Returns current render pass.
  //   virtual E_SCENE_NODE_RENDER_PASS getSceneNodeRenderPass() const override;

  //   //! Creates a new scene manager.
  //   virtual ISceneManager* createNewSceneManager(bool cloneContent) override;

  //   //! Returns type of the scene node
  //   virtual E_SCENE_NODE_TYPE getType() const override { return E_SCENE_NODE_TYPE::SCENE_MANAGER; }

  //   //! Returns the default scene node factory which can create all built in scene nodes
  //   virtual ISceneNodeFactory* getDefaultSceneNodeFactory() override;

  //   //! Adds a scene node factory to the scene manager.
  //   /** Use this to extend the scene manager with new scene node types which it should be
  //   able to create automatically, for example when loading data from xml files. */
  //   virtual void registerNodeFactory(ISceneNodeFactory* factoryToAdd) override;

  //   //! Returns amount of registered scene node factories.
  //   virtual std::uint32_t getRegisteredSceneNodeFactoryCount() const override;

  //   //! Returns a scene node factory by index
  //   virtual ISceneNodeFactory* getSceneNodeFactory(std::uint32_t index) override;

  //   //! Returns a typename from a scene node type or null if not found
  //   virtual const char* getSceneNodeTypeName(E_SCENE_NODE_TYPE type) override;

  //   //! Returns a typename from a scene node animator type or null if not found
  //   virtual const char* getAnimatorTypeName(ESCENE_NODE_ANIMATOR_TYPE type) override;

  //   //! Adds a scene node to the scene by name
  //   virtual ISceneNode* addSceneNode(const char* sceneNodeTypeName, ISceneNode* parent= 0) override;

  //   //! creates a scene node animator based on its type name
  //   virtual ISceneNodeAnimator* createSceneNodeAnimator(const char* typeName, ISceneNode* target= 0) override;

  //   //! Returns the default scene node animator factory which can create all built-in scene node animators
  //   virtual ISceneNodeAnimatorFactory* getDefaultSceneNodeAnimatorFactory() override;

  //   //! Adds a scene node animator factory to the scene manager.
  //   virtual void registerNodeAnimatorFactory(ISceneNodeAnimatorFactory* factoryToAdd) override;

  //   //! Returns amount of registered scene node animator factories.
  //   virtual std::uint32_t getRegisteredSceneNodeAnimatorFactoryCount() const override;

  //   //! Returns a scene node animator factory by index
  //   virtual ISceneNodeAnimatorFactory* getSceneNodeAnimatorFactory(std::uint32_t index) override;

  //   //! Saves the current scene into a file.
  //   // virtual bool saveScene(const std::string& filename, ISceneUserDataSerializer* userDataSerializer= 0, ISceneNode* node= 0) override;

  //   //! Saves the current scene into a file.
  //   // virtual bool saveScene(io::IWriteFile* file, ISceneUserDataSerializer* userDataSerializer= 0, ISceneNode* node= 0) override;

  //   //! Saves the current scene into a file.
  //   // virtual bool saveScene(io::IXMLWriter* writer, const std::string& currentPath, ISceneUserDataSerializer* userDataSerializer= 0, ISceneNode* node= 0) override;

  //   //! Loads a scene. Note that the current scene is not cleared before.
  //   virtual bool loadScene(const std::string& filename, ISceneUserDataSerializer* userDataSerializer= 0, ISceneNode* rootNode= 0) override;

  //   //! Loads a scene. Note that the current scene is not cleared before.
  //   virtual bool loadScene(io::IReadFile* file, ISceneUserDataSerializer* userDataSerializer= 0, ISceneNode* rootNode= 0) override;

  //   //! Writes attributes of the scene node.
  //   // virtual void serializeAttributes(io::IAttributes* out, io::SAttributeReadWriteOptions* options= 0) const override;

  //   // ! Reads attributes of the scene node.
  //   // virtual void deserializeAttributes(io::IAttributes* in, io::SAttributeReadWriteOptions* options= 0) override;

  //   //! Returns a mesh writer implementation if available
  //   // virtual IMeshWriter* createMeshWriter(EMESH_WRITER_TYPE type) override;

  //   //! Get a skinned mesh, which is not available as header-only code
  //   virtual ISkinnedMesh* createSkinnedMesh() override;

  //   //! Sets ambient color of the scene
  //   virtual void setAmbientLight(const video::SColorf &ambientColor) override;

  //   //! Returns ambient color of the scene
  //   virtual const video::SColorf& getAmbientLight() const override;

  //   //! Register a custom callbacks manager which gets callbacks during scene rendering.
  //   virtual void setLightManager(ILightManager* lightManager) override;

  //   //! Get current render time.
  //   virtual E_SCENE_NODE_RENDER_PASS getCurrentRenderPass() const override { return CurrentRenderPass; }

  //   //! Set current render time.
  //   virtual void setCurrentRenderPass(E_SCENE_NODE_RENDER_PASS nextPass) override { CurrentRenderPass = nextPass; }

  //   //! Get an instance of a geometry creator.
  //   virtual const IGeometryCreator* getGeometryCreator(void) const override { return GeometryCreator; }

  //   //! returns if node is culled
  //   virtual bool isCulled(const ISceneNode* node) const override;

  // private:

  //   // load and create a mesh which we know already isn't in the cache and put it in there
  //   IAnimatedMesh* getUncachedMesh(io::IReadFile* file, const std::string& filename, const std::string& cachename);

  //   //! clears the deletion list
  //   void clearDeletionList();

  //   //! writes a scene node
  //   void writeSceneNode(io::IXMLWriter* writer, ISceneNode* node, ISceneUserDataSerializer* userDataSerializer, const wchar_t* currentPath= 0, bool init=false);

  //   struct DefaultNodeEntry
  //   {
  //     DefaultNodeEntry(ISceneNode* n) :
  //       Node(n), TextureValue(0)
  //     {
  //       // if (n->getMaterialCount())
  //         // TextureValue = (n->getMaterial(0).getTexture(0));
  //     }

  //     bool operator < (const DefaultNodeEntry& other) const
  //     {
  //       return (TextureValue < other.TextureValue);
  //     }

  //     ISceneNode* Node;
  //     private:
  //     void* TextureValue;
  //   };

  //   //! sort on distance (center) to camera
  //   struct TransparentNodeEntry
  //   {
  //     TransparentNodeEntry(ISceneNode* n, const glm::vec3& camera)
  //       : Node(n)
  //     {
  //       Distance = Node->getAbsoluteTransformation().getTranslation().getDistanceFromSQ(camera);
  //     }

  //     bool operator < (const TransparentNodeEntry& other) const
  //     {
  //       return Distance > other.Distance;
  //     }

  //     ISceneNode* Node;
  //     private:
  //       double Distance;
  //   };

  //   //! sort on distance (sphere) to camera
  //   struct DistanceNodeEntry
  //   {
  //     DistanceNodeEntry(ISceneNode* n, const glm::vec3& cameraPos)
  //       : Node(n)
  //     {
  //       setNodeAndDistanceFromPosition(n, cameraPos);
  //     }

  //     bool operator < (const DistanceNodeEntry& other) const
  //     {
  //       return Distance < other.Distance;
  //     }

  //     void setNodeAndDistanceFromPosition(ISceneNode* n, const glm::vec3 & fromPosition)
  //     {
  //       Node = n;
  //       Distance = Node->getAbsoluteTransformation().getTranslation().getDistanceFromSQ(fromPosition);
  //       Distance -= Node->getBoundingBox().getExtent().getLengthSQ() * 0.5;
  //     }

  //     ISceneNode* Node;
  //     private:
  //     double Distance;
  //   };

    //! Video driver
    std::shared_ptr<video::IVideoDriver> Driver;

    //! ID for mesh
    ID MeshID = INVALID_ID;

    //! ID for texture
    ID TextureID = INVALID_ID;

    //! ID for scene node
    ID NodeID = INVALID_ID;

    std::shared_ptr<ISceneNode> RootSceneNode = nullptr;

    PassSceneNodeList NodeList;

    //! Mesh cache
    std::unordered_map<ID, std::shared_ptr<IMesh>> MeshCache;
  
    //! Scene node cache
    std::unordered_map<ID, std::shared_ptr<ISceneNode>> NodeCache;

    //! collision manager
    // ISceneCollisionManager* CollisionManager;

    //! render pass lists
    std::vector<std::shared_ptr<ICameraSceneNode>> CameraList;
    // std::vector<ISceneNode*> LightList;
    // std::vector<ISceneNode*> ShadowNodeList;
    // std::vector<ISceneNode*> SkyBoxList;
    // std::vector<DefaultNodeEntry> SolidNodeList;
    // std::vector<TransparentNodeEntry> TransparentNodeList;
    // std::vector<TransparentNodeEntry> TransparentEffectNodeList;

    std::vector<std::unique_ptr<IMeshLoader>> MeshLoaderList;
    // std::vector<ISceneLoader*> SceneLoaderList;
    // std::vector<ISceneNode*> DeletionList;
    // std::vector<ISceneNodeFactory*> SceneNodeFactoryList;
    // std::vector<ISceneNodeAnimatorFactory*> SceneNodeAnimatorFactoryList;

    //! Current active camera
    std::shared_ptr<ICameraSceneNode> ActiveCamera = nullptr;
    // glm::vec3 camWorldPos; // Position of camera for transparent nodes.

    //! String parameters
    // NOTE: Attributes are slow and should only be used for debug-info and not in release
    // io::CAttributes* Parameters;

    //! constants for reading and writing XML.
    //! Not made static due to portability problems.
    // const std::wstring IRR_XML_FORMAT_SCENE;
    // const std::wstring IRR_XML_FORMAT_NODE;
    // const std::wstring IRR_XML_FORMAT_NODE_ATTR_TYPE;

    // IGeometryCreator* GeometryCreator;
  };

} // namespace video
} // namespace scene

#endif

