// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "CSceneNodeAnimatorFlyStraight.h"

namespace saga
{
namespace scene
{


//! constructor
CSceneNodeAnimatorFlyStraight::CSceneNodeAnimatorFlyStraight(const glm::vec3& startPoint,
        const glm::vec3& endPoint, std::uint32_t timeForWay,
        bool loop, std::uint32_t now, bool pingpong)
: ISceneNodeAnimatorFinishing(now + timeForWay),
  Start(startPoint), End(endPoint), TimeFactor(0.0f),
  TimeForWay(timeForWay), Loop(loop), PingPong(pingpong)
{
  #ifdef _DEBUG
  setDebugName("CSceneNodeAnimatorFlyStraight");
  #endif

  StartTime = now;

  recalculateIntermediateValues();
}


void CSceneNodeAnimatorFlyStraight::recalculateIntermediateValues()
{
  Vector = End - Start;
  TimeFactor = (float)Vector.getLength() / TimeForWay;
  Vector.normalize();
}


//! animates a scene node
void CSceneNodeAnimatorFlyStraight::animateNode(ISceneNode* node, std::uint32_t timeMs)
{
  if (!node)
    return;

  std::uint32_t t = (timeMs-(StartTime+PauseTimeSum));

  glm::vec3 pos;

  if (!Loop && !PingPong && t >= TimeForWay)
  {
    pos = End;
    HasFinished = true;
  }
  else if (!Loop && PingPong && t >= TimeForWay * 2.f)
  {
    pos = Start;
    HasFinished = true;
  }
  else
  {
    float phase = fmodf((float) t, (float) TimeForWay);
    glm::vec3 rel = Vector * phase * TimeFactor;
    const bool pong = PingPong && fmodf((float) t, (float) TimeForWay*2.f) >= TimeForWay;

    if (!pong)
    {
      pos += Start + rel;
    }
    else
    {
      pos = End - rel;
    }
  }

  node->setPosition(pos);
}


//! Writes attributes of the scene node animator.
void CSceneNodeAnimatorFlyStraight::serializeAttributes(io::IAttributes* out, io::SAttributeReadWriteOptions* options) const
{
  ISceneNodeAnimatorFinishing::serializeAttributes(out, options);

  out->addVector3d("Start", Start);
  out->addVector3d("End", End);
  out->addInt("TimeForWay", TimeForWay);
  out->addBool("Loop", Loop);
  out->addBool("PingPong", PingPong);
}


//! Reads attributes of the scene node animator.
void CSceneNodeAnimatorFlyStraight::deserializeAttributes(io::IAttributes* in, io::SAttributeReadWriteOptions* options)
{
  ISceneNodeAnimatorFinishing::deserializeAttributes(in, options);

  Start = in->getAttributeAsVector3d("Start");
  End = in->getAttributeAsVector3d("End");
  TimeForWay = in->getAttributeAsInt("TimeForWay");
  Loop = in->getAttributeAsBool("Loop");
  PingPong = in->getAttributeAsBool("PingPong");

  recalculateIntermediateValues();
}


ISceneNodeAnimator* CSceneNodeAnimatorFlyStraight::createClone(ISceneNode* node, ISceneManager* newManager)
{
  CSceneNodeAnimatorFlyStraight * newAnimator =
    new CSceneNodeAnimatorFlyStraight(Start, End, TimeForWay, Loop, StartTime, PingPong);
  newAnimator->cloneMembers(this);

  return newAnimator;
}


} // namespace scene
} // namespace saga

