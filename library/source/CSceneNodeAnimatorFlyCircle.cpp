// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "CSceneNodeAnimatorFlyCircle.h"

namespace saga
{
namespace scene
{


//! constructor
CSceneNodeAnimatorFlyCircle::CSceneNodeAnimatorFlyCircle(std::uint32_t time,
    const glm::vec3& center, float radius, float speed,
    const glm::vec3& direction, float radiusEllipsoid)
  : Center(center), Direction(direction), Radius(radius),
  RadiusEllipsoid(radiusEllipsoid), Speed(speed)
{
  #ifdef _DEBUG
  setDebugName("CSceneNodeAnimatorFlyCircle");
  #endif

  StartTime = time;

  init();
}


void CSceneNodeAnimatorFlyCircle::init()
{
  Direction.normalize();

  if (Direction.Y != 0)
    VecV = glm::vec3(50,0,0).crossProduct(Direction).normalize();
  else
    VecV = glm::vec3(0,50,0).crossProduct(Direction).normalize();
  VecU = VecV.crossProduct(Direction).normalize();
}


//! animates a scene node
void CSceneNodeAnimatorFlyCircle::animateNode(ISceneNode* node, std::uint32_t timeMs)
{
  if (0 == node)
    return;

  float time;

  // Check for the condition where the StartTime is in the future.
  if(StartTime+PauseTimeSum > timeMs)
    time = ((std::int32_t)timeMs - (std::int32_t)(StartTime+PauseTimeSum)) * Speed;
  else
    time = (timeMs-(StartTime+PauseTimeSum)) * Speed;

//  node->setPosition(Center + Radius * ((VecU*cosf(time)) + (VecV*sinf(time))));
  float r2 = RadiusEllipsoid == 0.f ? Radius : RadiusEllipsoid;
  node->setPosition(Center + (Radius*cosf(time)*VecU) + (r2*sinf(time)*VecV));
}


//! Writes attributes of the scene node animator.
void CSceneNodeAnimatorFlyCircle::serializeAttributes(io::IAttributes* out, io::SAttributeReadWriteOptions* options) const
{
  ISceneNodeAnimator::serializeAttributes(out, options);

  out->addVector3d("Center", Center);
  out->addFloat("Radius", Radius);
  out->addFloat("Speed", Speed);
  out->addVector3d("Direction", Direction);
  out->addFloat("RadiusEllipsoid", RadiusEllipsoid);
}


//! Reads attributes of the scene node animator.
void CSceneNodeAnimatorFlyCircle::deserializeAttributes(io::IAttributes* in, io::SAttributeReadWriteOptions* options)
{
  ISceneNodeAnimator::deserializeAttributes(in, options);

  Center = in->getAttributeAsVector3d("Center");
  Radius = in->getAttributeAsFloat("Radius");
  Speed = in->getAttributeAsFloat("Speed");
  Direction = in->getAttributeAsVector3d("Direction");

  if (Direction.equals(glm::vec3(0,0,0)))
    Direction.set(0,1,0); // irrlicht 1.1 backwards compatibility
  else
    Direction.normalize();

  RadiusEllipsoid = in->getAttributeAsFloat("RadiusEllipsoid");
  init();
}


ISceneNodeAnimator* CSceneNodeAnimatorFlyCircle::createClone(ISceneNode* node, ISceneManager* newManager)
{
  CSceneNodeAnimatorFlyCircle * newAnimator =
    new CSceneNodeAnimatorFlyCircle(StartTime, Center, Radius, Speed, Direction, RadiusEllipsoid);
  newAnimator->cloneMembers(this);

  return newAnimator;
}


} // namespace scene
} // namespace saga

