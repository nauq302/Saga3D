// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __SK_2D_VERTEX_H_INCLUDED__
#define __SK_2D_VERTEX_H_INCLUDED__

#include "vector2d.h"

typedef signed short TZBufferType;

namespace saga
{
namespace video
{

  struct S2DVertex
  {
    core::vector2d<std::int32_t> Pos;  // position
    core::vector2d<std::int32_t> TCoords;  // texture coordinates
    TZBufferType ZValue;    // zvalue
    std::uint16_t Color;
  };


} // namespace video
} // namespace saga

#endif

