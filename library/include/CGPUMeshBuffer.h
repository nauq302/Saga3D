#ifndef __CGPU_MESH_BUFFER_H_INCLUDED__
#define __CGPU_MESH_BUFFER_H_INCLUDED__

#include "IMeshBuffer.h"
#include "SShaderBuffer.h"

namespace saga
{
namespace scene
{
  //! Implementation of the CPU IMeshBuffer interface
  class CGPUMeshBuffer : public IMeshBuffer
  {
  public:
    //! Default constructor for empty meshbuffer
    CGPUMeshBuffer()
      :  ID(++RootID)
    {

    }

    virtual bool isGPUBuffer() const override { return true; }

    virtual ~CGPUMeshBuffer() {}

    virtual std::uint64_t getID() const override { return ID; }

    virtual void buildBuffer(video::IVideoDriver& driver, const video::PipelineHandle pipeline) override {}

    //! Get pointer to vertices
    /** \return Pointer to vertices. */
    virtual void* getVertices() override { return nullptr; }

    //! Get pointer to GPU staging buffer
    /** \return Pointer to staging buffer. */
    virtual const void* getData(const video::PipelineHandle pipeline) const override { return nullptr; }

    //! Get size of GPU staging buffer
    /** \return Size of staging buffer. */
    virtual std::size_t getSize(const video::PipelineHandle pipeline) const override { return 0; }

    //! Set a shader buffer as MeshBuffer
    /** For rendering from a buffer manipulated by shaders */
    void setVertexBuffer(const video::ShaderBufferHandle buffer) { VertexBuffer = buffer; }

    //! Get GPU mesh buffer
    /** Return GPU mesh buffer handle (ie: bind for rendering) */
    auto getVertexBuffer() const { return VertexBuffer; }

    //! Set number of vertices
    virtual void setVertexCount(const size_t count) override { VertexCount = count; }

    //! Get number of vertices
    /** \return Number of vertices. */
    virtual std::size_t getVertexCount() const override { return VertexCount; }

    //! Set a shader buffer as vertex buffer
    /** For rendering from a buffer manipulated by shaders */
    void setIndexBuffer(const video::ShaderBufferHandle buffer) { IndexBuffer = buffer; }

    //! Get GPU index buffer
    /** Return GPU index buffer handle (ie: bind for rendering) */
    auto getIndexBuffer() const { return IndexBuffer; }

    //! Set number of indices
    virtual void setIndexCount(const std::uint32_t count) { IndexCount = count; }

    //! Get pointer to indices
    /** \return Pointer to indices. */
    virtual const std::uint32_t* getIndices() const override { return nullptr; }

    //! Get pointer to indices
    /** \return Pointer to indices. */
    virtual std::uint32_t* getIndices() override {return nullptr;}

    //! returns vertex index at this offset
    virtual std::uint32_t getIndex(std::size_t offset) const override { return 0; }

    //! Get number of indices
    /** \return Number of indices. */
    virtual std::uint32_t getIndexCount() const override { return IndexCount; }

    //! Get the axis aligned bounding box
    /** \return Axis aligned bounding box of this buffer. */
    virtual const core::aabbox3d<float>& getBoundingBox() const override { return {}; }

    //! Set the axis aligned bounding box
    /** \param box New axis aligned bounding box for this buffer. */
    //! set user axis aligned bounding box
    virtual void setBoundingBox(const core::aabbox3df& box) override {}

    //! Recalculate the bounding box.
    /** should be called if the mesh changed. */
    virtual void recalculateBoundingBox() override {}

    //! returns position of vertex i
    virtual const glm::vec3& getPosition(std::uint32_t i) const override { return {}; }

    //! returns normal of vertex i
    virtual const glm::vec3& getNormal(std::uint32_t i) const override { return {}; }

    //! returns texture coord of vertex i
    virtual const glm::vec2& getTCoords(std::uint32_t i) const override { return {}; }

    //! returns tangent of vertex i
    virtual const glm::vec3& getTangent(std::uint32_t i) const override { return {}; }

    //! returns bi-tangent of vertex i
    virtual const glm::vec3& getBiTangent(std::uint32_t i) const override { return {}; }

    //! Append the vertices and indices to the current buffer
    /** Only works for compatible types, i.e. either the same type
    or the main buffer is of standard type. Otherwise, behavior is
    undefined.
    */
    virtual void append(std::vector<S3DVertex>&& vertices, std::vector<uint32_t>&& indices) override {}

    //! Append the custom attribute buffer to the current mesh buffer
    /**
    \param buffer Pointer to the buffer containing data
    \param size Total size of buffer (bytes)
    \param stride The custom data size of each vertex (bytes) */
    virtual void appendAttribute(const char* buffer, const size_t size, const size_t stride) override {}

    //! Append the meshbuffer to the current buffer
    /** Only works for compatible types, i.e. either the same type
    or the main buffer is of standard type. Otherwise, behavior is
    undefined.
    \param other Meshbuffer to be appended to this one.
    */
    virtual void append(const IMeshBuffer* const other) override {}

    const std::uint64_t ID;

    video::ShaderBufferHandle VertexBuffer = video::NULL_GPU_RESOURCE_HANDLE;
    std::size_t VertexCount = 0;

    video::ShaderBufferHandle IndexBuffer = video::NULL_GPU_RESOURCE_HANDLE;
    std::uint32_t IndexCount = 0;
  };

} // namespace scene
} // namespace saga

#endif


