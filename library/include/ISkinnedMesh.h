#ifndef __I_SKINNED_MESH_H_INCLUDED__
#define __I_SKINNED_MESH_H_INCLUDED__

#include "IAnimatedMesh.h"

namespace saga
{
namespace scene
{

  //! Interface for using some special functions of Skinned meshes
  class ISkinnedMesh : public IAnimatedMesh
  {
  public:

    //! Gets joint count.
    /** \return Amount of joints in the skeletal animated mesh. */
    virtual std::uint32_t getBoneCount() const = 0;

    //! Gets transformation matrix of bone
    /** \return 4x4 matrix representing bone transformation in current animation. */
    virtual glm::mat4 getBoneTransform(const std::uint32_t boneID) const = 0;

    //! Gets the name of a joint.
    /** \param number: Zero based index of joint. The last joint
    has the number getBoneCount()-1;
    \return Name of joint and null if an error happened. */
    // virtual const std::string& getJointName(std::uint32_t index) const = 0;

    //! Gets a joint number from its name
    /** \param name: Name of the joint.
    \return Number of the joint or -1 if not found. */
    // virtual std::int32_t getBoneID(const std::string& name) const = 0;

    //! Use animation from another mesh
    /** The animation is linked (not copied) based on joint names
    so make sure they are unique.
    \return True if all joints in this mesh were
    matched up (empty names will not be matched, and it's case
    sensitive). Unmatched joints will not be animated. */
    // virtual bool useAnimationFrom(const ISkinnedMesh* mesh) = 0;

  };

} // namespace scene
} // namespace saga

#endif // __I_SKINNED_MESH_H_INCLUDED__
