#ifndef __SAGA_H_INCLUDED__
#define __SAGA_H_INCLUDED__

#include "SagaConfig.h"
#include "aabbox3d.h"
#include "CMeshBuffer.h"
#include "CGPUMeshBuffer.h"
#include "IAnimatedMesh.h"
#include "IAnimatedMeshSceneNode.h"
#include "ICameraSceneNode.h"
#include "IMesh.h"
#include "IMeshBuffer.h"
#include "IMeshSceneNode.h"
#include "SagaDevice.h"
#include "irrMath.h"
#include "ISceneManager.h"
#include "ISceneNode.h"
#include "ISkinnedMesh.h"
#include "IVideoDriver.h"
#include "SShader.h"
#include "SRenderPass.h"
#include "SRenderPassState.h"
#include "S3DVertex.h"
#include "SMesh.h"

/*! \mainpage Saga3D 1.0-alpha API documentation
 *
 * <div align="center"><img src="logo.png" ></div>
 *
 * \section intro Introduction
 *
 * Welcome to the Saga3D API documentation.
 *
 * From this page you can find all necessary information to develop with Saga3D
 * including: Guide, Tutorial, Note, ... and of course the detailed API documentation.
 *
 * \section example Basic usage
 *
 * \code
 * #include <Saga.h>
 *
 * using namespace saga;
 *
 * int main() {
 *   // initialize
 *   auto device = createDevice(video::E_DRIVER_TYPE::VULKAN, {800, 600});
 *
 *   // grab important objects
 *   const auto& driver = device->getVideoDriver();
 *   const auto& smgr = device->getSceneManager();
 *
 *   // set window title
 *   device->setWindowCaption("Saga3D Application");
 *
 *   // create and set up render passes, pipelines
 *
 *   // load meshes and textures
 *
 *   // create scene nodes from loaded data
 *
 *   // set pipeline for scene nodes
 *
 *   // register scene nodes to render passes
 *
 *   // add a static camera
 *   smgr->addCameraSceneNode();
 *
 *   // render the scene
 *   while(device->run())
 *   {
 *      smgr->animate(); // animate cameras and scene nodes
 *
 *      driver->begin();         // begin command buffer recording
 *      driver->beginPass(...);  // start the first render pass
 *      driver->endPass();       // end render pass
 *      driver->beginPass(...);  // start the second render pass, if there is
 *      driver->endPass();       // end render pass
 *      driver->end();           // end command buffer recording
 *
 *      driver->submit();  // submit and wait for GPU execution
 *      driver->present(); // show rendered results on screen
 *   }
 *
 *   return 0;
 * }
 * \endcode
 *
 */

#include "SDeviceCreationParameters.h"

namespace saga
{
  //! Creates an Saga device. The Saga device is the root object for using the engine.
  /** If you need more parameters to be passed to the creation of the Saga device,
  use the createDeviceEx() function.
  \param driverType: Type of the video driver to use. This can currently be video::E_DRIVER_TYPE::NULL_DRIVER,
  video::E_DRIVER_TYPE::SOFTWARE, video::E_DRIVER_TYPE::BURNINGSVIDEO, video::E_DRIVER_TYPE::DIRECT3D9 and video::E_DRIVER_TYPE::OPENGL.
  \param windowSize: Size of the window or the video mode in fullscreen mode.
  \param bits: Bits per pixel in fullscreen mode. Ignored if windowed mode.
  \param fullscreen: Should be set to true if the device should run in fullscreen. Otherwise
    the device runs in windowed mode.
  \param stencilbuffer: Specifies if the stencil buffer should be enabled. Set this to true,
  if you want the engine be able to draw stencil buffer shadows. Note that not all
  devices are able to use the stencil buffer. If they don't no shadows will be drawn.
  \param vsync: Specifies vertical synchronization: If set to true, the driver will wait
  for the vertical retrace period, otherwise not.
  \param receiver: A user created event receiver.
  \return Returns pointer to the created SagaDevice or null if the
  device could not be created.
  */
  std::unique_ptr<SagaDevice> createDevice(
    video::E_DRIVER_TYPE driverType = video::E_DRIVER_TYPE::VULKAN,
    // parentheses are necessary for some compilers
    const glm::uvec2& windowSize = {800, 600},
    std::uint32_t bits = 32,
    bool fullscreen = false,
    bool stencilbuffer = true,
    bool vsync = false
 );

  //! typedef for Function Pointer
//   typedef SagaDevice* (*funcptr_createDevice)(
//     video::E_DRIVER_TYPE driverType,
//     const glm::uvec2& windowSize,
//     std::uint32_t bits,
//     bool fullscreen,
//     bool stencilbuffer,
//     bool vsync
//  );

  //! Creates an Saga device with the option to specify advanced parameters.
  /** Usually you should used createDevice() for creating an Saga device.
  Use this function only if you wish to specify advanced parameters like a window
  handle in which the device should be created.
  \param parameters: Structure containing advanced parameters for the creation of the device.
  See saga::SDeviceCreationParameters for details.
  \return Returns pointer to the created IrrlichtDevice or null if the
  device could not be created. */
  std::unique_ptr<SagaDevice> createDeviceEx(
    const SDeviceCreationParameters& parameters);

  //! typedef for Function Pointer
  // typedef SagaDevice* (*funcptr_createDeviceEx)(const SDeviceCreationParameters& parameters);

  // THE FOLLOWING IS AN EMPTY LIST OF ALL SUB NAMESPACES
  // EXISTING ONLY FOR THE DOCUMENTATION SOFTWARE DOXYGEN.

  //! Basic classes such as vectors, planes, arrays, lists, and so on can be found in this namespace.
  namespace core
  {
  }

  //! All scene management can be found in this namespace: Mesh loading, special scene nodes like octrees and billboards, ...
  namespace scene
  {
  }

  //! The video namespace contains classes for accessing the video driver. All 2d and 3d rendering is done here.
  namespace video
  {
  }
}

/*! \file Saga.h
  \brief Main header file of Saga3D, the only file needed to include.
*/

#endif // __SAGA_H_INCLUDED__

