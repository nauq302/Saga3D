#ifndef __I_VIDEO_DRIVER_H_INCLUDED__
#define __I_VIDEO_DRIVER_H_INCLUDED__

#include "SRenderPass.h"
#include "SPipeline.h"
#include "SShaderUniform.h"
#include "SPushConstant.h"
#include "SShaderBuffer.h"
#include "SIndirectBuffer.h"
#include "ISceneNode.h"
#include <vector>
#include <string>
#include <glm/vec2.hpp>

namespace saga
{

class SagaDevice;

namespace scene
{
  class IMeshBuffer;
  class ISceneManager;
  class IMeshSceneNode;
}

namespace video
{

  //! Special render targets, which usually map to dedicated hardware
  /** These render targets (besides 0 and 1) need not be supported by gfx cards */
  enum class E_RENDER_TARGET_TYPE
  {
    //! Render target is the main color frame buffer
    FRAME_BUFFER= 0,
    //! Render target is a render texture
    RENDER_TEXTURE,
    //! Multi-Render target textures
    MULTI_RENDER_TEXTURES,
    //! Render target is the main color frame buffer
    STEREO_LEFT_BUFFER,
    //! Render target is the right color buffer (left is the main buffer)
    STEREO_RIGHT_BUFFER,
    //! Render to both stereo buffers at once
    STEREO_BOTH_BUFFERS,
    //! Auxiliary buffer 0
    AUX_BUFFER0,
    //! Auxiliary buffer 1
    AUX_BUFFER1,
    //! Auxiliary buffer 2
    AUX_BUFFER2,
    //! Auxiliary buffer 3
    AUX_BUFFER3,
    //! Auxiliary buffer 4
    AUX_BUFFER4
  };

  //! enum class for the types of fog distributions to choose from
  enum class E_FOG_TYPE
  {
    EXP,
    LINEAR,
    EXP2
  };

  enum class E_SHADER_TYPE;

  class IVideoDriver
  {
  public:
    virtual const std::string& getVendorName() const = 0;

    virtual void begin() = 0;
    virtual void beginPass(RenderPassHandle pass) = 0;
    virtual void draw() = 0;
    virtual void submit() = 0;
    virtual void present(TextureHandle texture = NULL_GPU_RESOURCE_HANDLE) = 0;
    virtual void endPass() = 0;
    virtual void end() = 0;

    virtual void setPersistence(const SGPUResource::HandleType resource, const bool persistence = true) = 0;
    virtual void resetPersistence() = 0;
    virtual void clear() = 0;

    #ifdef SAGA_EXTRA_API_MEMORY_INFO
    virtual std::uint64_t getTotalMemory() const = 0;
    virtual std::uint64_t getAllocatedMemory() const = 0;
    #endif

    virtual void enqueuePass(const RenderPassHandle pass) = 0;
    virtual void render() = 0;

    virtual SRenderPass createRenderPass() const = 0;
    virtual SGPUResource::HandleType createResource(SRenderPass&& pass) = 0;
    virtual SRenderPass& getRenderPass(const RenderPassHandle pass) = 0;
    virtual void destroyRenderPass(const RenderPassHandle pass) = 0;

    virtual SPipeline createPipeline() const = 0;
    virtual SGPUResource::HandleType createResource(SPipeline&& pipeline) = 0;
    virtual SPipeline& getPipeline(const PipelineHandle p) = 0;
    virtual void destroyPipeline(const PipelineHandle p) = 0;

    virtual void bindComputePipeline(const PipelineHandle& compute) = 0;
    virtual void dispatchComputePipeline(std::uint32_t x, std::uint32_t y, uint32_t z) = 0;

    virtual STexture createTexture() = 0;
    virtual void loadTexture(STexture& texture, const int face, const int level, const std::string& path) const = 0;
    virtual void loadTexture(STexture& texture, const int face, const int level,
      const unsigned char* data, const size_t size) const = 0;

    virtual TextureHandle createTexture(STexture&& texture) = 0;
    virtual TextureHandle createTexture(const std::string& path, const E_PIXEL_FORMAT format = E_PIXEL_FORMAT::RGBA8) = 0;
    virtual TextureHandle createTexture(unsigned char* fileData, const std::size_t size, const E_PIXEL_FORMAT format = E_PIXEL_FORMAT::RGBA8) = 0;
    virtual TextureHandle createTexture(unsigned char* pixelData, const int width, const int height, const E_PIXEL_FORMAT format = E_PIXEL_FORMAT::RGBA8) = 0;
    virtual void bindTexture(TextureHandle texture, const int binding) = 0;

    virtual STexture& getTexture(const TextureHandle texture) = 0;
    virtual STexture& getTextureByID(const ID id) = 0;
    virtual void destroyTexture(const TextureHandle texture) = 0;
  
    virtual SShader createShader() const = 0;
    virtual ShaderHandle createResource(SShader&& shader) = 0;
    virtual SShader& getShader(const ShaderHandle shader) = 0;
    virtual void destroyShader(const ShaderHandle shader) = 0;

    virtual SShaderUniform createShaderUniform() const = 0;
    virtual ShaderUniformHandle createResource(SShaderUniform&& shader) = 0;
    virtual SShaderUniform& getShaderUniform(const ShaderUniformHandle uniform) = 0;
    virtual void destroyShaderUniform(const ShaderUniformHandle uniform) = 0;

    virtual void updateShaderUniform(const ShaderUniformHandle uniform, const void* data) = 0;
    virtual void bindShaderUniform(const ShaderUniformHandle uniform, const int binding) = 0;

    virtual SPushConstant createPushConstant() const = 0;
    virtual PushConstantHandle createResource(SPushConstant&& con) = 0;
    virtual SPushConstant& getPushConstant(const PushConstantHandle con) = 0;
    virtual void destroyPushConstant(const PushConstantHandle con) = 0;

    virtual void updatePushConstant(const PushConstantHandle con, const void* data, const size_t offset = 0, const size_t size = 0) = 0;

    virtual SShaderBuffer createShaderBuffer() const = 0;
    virtual ShaderBufferHandle createResource(SShaderBuffer&& buffer) = 0;
    virtual SShaderBuffer& getShaderBuffer(const ShaderBufferHandle buffer) = 0;
    virtual void destroyShaderBuffer(const ShaderBufferHandle buffer) = 0;

    virtual void updateShaderBuffer(const ShaderBufferHandle buffer, const void* data, const size_t offset = 0, const size_t size = 0) = 0;
    virtual void bindShaderBuffer(const ShaderBufferHandle buffer, const int binding) = 0;

    virtual void addShader(SShader& shader, E_SHADER_TYPE type, std::string&& source) const = 0;
    virtual void addShaderFromFile(SShader& shader, E_SHADER_TYPE type, const std::string& path) const = 0;

    virtual SIndirectBuffer createIndirectBuffer() = 0;
    virtual IndirectBufferHandle createResource(SIndirectBuffer&& buffer) = 0;
    virtual SIndirectBuffer& getIndirectBuffer(const IndirectBufferHandle buffer) = 0;
    virtual void destroyIndirectBuffer(const IndirectBufferHandle buffer) = 0;

    virtual SIndexedIndirectBuffer createIndexedIndirectBuffer() = 0;
    virtual IndexedIndirectBufferHandle createResource(SIndexedIndirectBuffer&& buffer) = 0;
    virtual SIndexedIndirectBuffer& getIndexedIndirectBuffer(const IndexedIndirectBufferHandle buffer) = 0;
    virtual void destroyIndexedIndirectBuffer(const IndexedIndirectBufferHandle buffer) = 0;

    virtual void* mapBuffer(const ShaderBufferHandle buffer,
      const std::size_t offset = 0, const std::size_t size = 0) const = 0;
    virtual void unmapBuffer(const ShaderBufferHandle buffer) const = 0;

    virtual void createPipelineBuffer(const scene::IMeshSceneNode& node) = 0;
    virtual void destroyPipelineBuffer(const scene::ISceneNode& node, bool destroyAll = false) = 0;

    //! Get window's width
    virtual std::uint32_t getWidth() const = 0;

    //! Get window's height
    virtual std::uint32_t getHeight() const = 0;

    virtual void setSceneManager(const std::shared_ptr<scene::ISceneManager>& smgr) = 0;

    //! Copy texture content to another texture
    /**
      \param srcTex source texture handle
      \param dstTex destination texture handle
      \param srcOffset range of source texture to copy, default is (width, height) of srcTex
      \param dstOffset range of destination texture to be written, default is (width, height) of dstTex
    */
    virtual void copyTexture(TextureHandle srcTex, TextureHandle dstTex,
      const glm::ivec2& srcOffset = {}, const glm::ivec2& dstOffset = {}, const glm::ivec2& size = {}) = 0;

    //! Copy texture content a buffer
    /**
      \param srcTex source texture handle
      \param dstBuffer destination buffer handle
      \param srcOffset range of source texture to copy, default is (width, height) of srcTex
      \param dstOffset range of destination texture to be written, default is (width, height) of dstTex
    */
    virtual void copyTextureToBuffer(TextureHandle srcTex, ShaderBufferHandle dstBuffer,
      const glm::ivec2& srcOffset = {}, const glm::ivec2& dstOffset = {}, const glm::ivec2& size = {}) = 0;

    //! Blit texture content to another texture
    /** Format conversion may perform
      \param srcTex source texture handle
      \param dstTex destination texture handle
      \param srcOffset range of source texture to blit, default is (width, height) of srcTex
      \param dstOffset range of destination texture to be written, default is (width, height) of dstTex
    */
    virtual void blitTexture(TextureHandle srcTex, TextureHandle dstTex,
      const glm::ivec2& srcOffset = {}, const glm::ivec2& dstOffset = {}) = 0;
  };

} // namespace video
} // namespace saga


#endif
