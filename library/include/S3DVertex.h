#ifndef __S3D_VERTEX_H_INCLUDED__
#define __S3D_VERTEX_H_INCLUDED__

#include "SColor.h"
#include "GraphicsConstants.h"
#include <array>
#include <vector>

namespace saga
{
namespace scene
{

//! standard vertex used by the Irrlicht engine.
struct S3DVertex
{
  //! default constructor
  S3DVertex() {}

  S3DVertex(float x, float y, float z)
    : Position(x, y, z) {}

  S3DVertex(const glm::vec3& Position, const glm::vec3& normal,
    glm::vec4 color, const glm::vec2& TextureCoords)
    : Position(Position), Normal(normal), Color(color), TextureCoords(TextureCoords) {}

  S3DVertex(const glm::vec3& Position, const glm::vec3& normal,
    glm::vec4 color, const glm::vec2& TextureCoords, const glm::vec3& tangent, const glm::vec3& bitangent)
    : Position(Position), Normal(normal), Color(color), TextureCoords(TextureCoords), Tangent(tangent), BiTangent(bitangent) {}

  glm::vec3 Position;
  glm::vec3 Normal;
  glm::vec4 Color;
  glm::vec2 TextureCoords;
  glm::vec3 Tangent;
  glm::vec3 BiTangent;
  glm::vec4 BoneWeights;
  glm::vec4 BoneIDs;
  std::vector<unsigned char> Attributes;

  bool operator==(const S3DVertex& other) const
  {
    return ((Position == other.Position) && (Normal == other.Normal) &&
      (Color == other.Color) && (TextureCoords == other.TextureCoords)) &&
      (Tangent == other.Tangent) && (BiTangent == other.BiTangent);
  }

  bool operator!=(const S3DVertex& other) const
  {
    return ((Position != other.Position) || (Normal != other.Normal) ||
      (Color != other.Color) || (TextureCoords != other.TextureCoords)) ||
      (Tangent != other.Tangent) || (BiTangent != other.BiTangent);
  }

  bool operator<(const S3DVertex& other) const
  {
    return (glm::all(glm::lessThan(Position, other.Position)) ||
        ((glm::all(glm::equal(Position, other.Position)) && glm::all(glm::lessThan(Normal, other.Normal))) ||
        (glm::all(glm::equal(Position, other.Position)) && glm::all(glm::equal(Normal, other.Normal)) && glm::all(glm::lessThan(Color, other.Color))) ||
        (glm::all(glm::equal(Position, other.Position)) && glm::all(glm::equal(Normal, other.Normal)) && glm::all(glm::equal(Color, other.Color)) &&
        glm::all(glm::lessThan(TextureCoords, other.TextureCoords)))));
  }

  S3DVertex getInterpolated(const S3DVertex& other, float d)
  {
    d = glm::clamp(d, 0.0f, 1.0f);
    return S3DVertex(glm::mix(Position, other.Position, d),
        glm::mix(Normal, other.Normal, d),
        glm::mix(Color, other.Color, d),
        glm::mix(TextureCoords, other.TextureCoords, d),
        glm::mix(Tangent, other.Tangent, d),
        glm::mix(BiTangent, other.BiTangent, d));
  }
};

} // namespace video
} // namespace saga

#endif

