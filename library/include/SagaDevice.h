#ifndef __SAGA_DEVICE_H_INCLUDED__
#define __SAGA_DEVICE_H_INCLUDED__

#include "EDriverTypes.h"
#include "EDeviceTypes.h"
#include <glm/vec2.hpp>
#include <cstdint>
#include <string>
#include <memory>

namespace saga
{
  class IEventReceiver;
  class IRandomizer;

  namespace io {
    class IFileSystem;
  } // namespace io

  namespace scene {
    class ISceneManager;
  } // namespace scene

  namespace video {
    enum class E_PIXEL_FORMAT;
    class IVideoDriver;
  } // namespace video

  //! The Irrlicht device. You can create it with createDevice() or createDeviceEx().
  /** This is the most important class of the Irrlicht Engine. You can
  access everything in the engine if you have a pointer to an instance of
  this class.  There should be only one instance of this class at any
  time.
  */
  class SagaDevice
  {
  public:

    virtual ~SagaDevice() {}

    //! Runs the device.
    /** Also increments the virtual timer by calling
    ITimer::tick();. You can prevent this
    by calling ITimer::stop(); before and ITimer::start() after
    calling IrrlichtDevice::run(). Returns false if device wants
    to be deleted. Use it in this way:
    \code
    while(device->run())
    {
      // draw everything here
    }
    \endcode
    If you want the device to do nothing if the window is inactive
    (recommended), use the slightly enhanced code shown at isWindowActive().

    Note if you are running Irrlicht inside an external, custom
    created window: Calling Device->run() will cause Irrlicht to
    dispatch windows messages internally.
    If you are running Irrlicht in your own custom window, you can
    also simply use your own message loop using GetMessage,
    DispatchMessage and whatever and simply don't use this method.
    But note that Irrlicht will not be able to fetch user input
    then. See saga::SDeviceCreationParameters::WindowId for more
    information and example code.
    */
    virtual bool run() = 0;

    //! Cause the device to temporarily pause execution for 500ms and let other processes run.
    /** This should bring down processor usage without major
    performance loss */
    virtual void yield() = 0;

    //! Pause execution and let other processes to run for a specified amount of time.
    /** It may not wait the full given time, as sleep may be interrupted
    \param timeMs: Time to sleep for in milliseconds.
    */
    virtual void sleep(std::uint32_t timeMs) = 0;

    //! Returns time elapsed in milliseconds since initialization
    /** \return Time in milliseconds  */
    virtual std::uint32_t getTime() const = 0;

    //! Provides access to the video driver for drawing 3d and 2d geometry.
    /** \return Pointer the video driver. */
    virtual const std::shared_ptr<video::IVideoDriver>& getVideoDriver() const = 0;

    //! Provides access to the scene manager.
    /** \return Pointer to the scene manager. */
    virtual const std::shared_ptr<scene::ISceneManager>& getSceneManager() const = 0;

    //! Register your event listener, which receives SDL_Event
    /**
    \param receiver: object whose class is derived from IEventReceiver
    */
    virtual void addEventReceiver(IEventReceiver* receiver) = 0;

    //! Sets the caption of the window.
    /** \param text: New text of the window caption. */
    virtual void setWindowCaption(const std::string& text) = 0;

    //! Returns if the window is active.
    /** If the window is inactive,
    nothing needs to be drawn. So if you don't want to draw anything
    when the window is inactive, create your drawing loop this way:
    \code
    while(device->run())
    {
      if (device->isWindowActive())
      {
        // draw everything here
      }
      else
        device->yield();
    }
    \endcode
    \return True if window is active. */
    virtual bool isWindowActive() const = 0;

    //! Checks if the Irrlicht window has focus
    /** \return True if window has focus. */
    virtual bool isWindowFocused() const = 0;

    //! Checks if the Irrlicht window is minimized
    /** \return True if window is minimized. */
    virtual bool isWindowMinimized() const = 0;

    //! Checks if the Irrlicht window is running in fullscreen mode
    /** \return True if window is fullscreen. */
    virtual bool isFullscreen() const = 0;

    //! Notifies the device that it should close itself.
    /** IrrlichtDevice::run() will always return false after closeDevice() was called. */
    virtual void closeDevice() = 0;

    //! Sets if the window should be resizable in windowed mode.
    /** The default is false. This method only works in windowed
    mode.
    \param resize Flag whether the window should be resizable. */
    virtual void setResizable(bool resize=false) = 0;

    //! Get window's width
    virtual std::uint32_t getWidth() const = 0;

    //! Get window's height
    virtual std::uint32_t getHeight() const = 0;

    //! Get aspect ratio
    virtual float getAspectRatio() const = 0;

    //! Resize the render window.
    /**  This will only work in windowed mode and is not yet supported on all systems.
    It does set the drawing/clientDC size of the window, the window decorations are added to that.
    You get the current window size with IVideoDriver::getScreenSize() (might be unified in future)
    */
    virtual void setWindowSize(const glm::uvec2& size) = 0;

    //! Minimizes the window if possible.
    virtual void minimizeWindow() = 0;

    //! Maximizes the window if possible.
    virtual void maximizeWindow() = 0;

    //! Restore the window to normal size if possible.
    virtual void restoreWindow() = 0;

    //! Get the position of the frame on-screen
    virtual glm::ivec2 getWindowPosition() = 0;
  };

} // namespace saga

#endif // __SAGA_DEVICE_H_INCLUDED__
