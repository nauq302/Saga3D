#ifndef __SPIPELINE_H_INCLUDED__
#define __SPIPELINE_H_INCLUDED__

#include "SShader.h"
#include "EPrimitiveTypes.h"
#include "SBlendState.h"
#include "SDepthStencilState.h"
#include "SRasterizerState.h"
#include "SPipelineLayout.h"

namespace saga
{
namespace video
{
  struct SPipeline : public SGPUResource
  {
    ShaderHandle Shaders;
    scene::E_PRIMITIVE_TYPE PrimitiveType;
    SRasterizerState Rasterizer;
    SDepthStencilState DepthStencil;
    SBlendState Blend;
    SPipelineLayout Layout;
    bool isCompute = false;
    int VertexBindingCount = 1;
  };

  using PipelineHandle = SGPUResource::HandleType;

} // namespace scene
} // namespace saga

#endif // __SPIPELINE_H_INCLUDED__

