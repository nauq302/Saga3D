# Project Saga3D, developed by [PRIME-tech](https://prime-tech.io/open-source/)

[Discord](https://discord.gg/QdY7tuJ)

[Documentation](https://saga.prime-tech.io)
## Goals
Provide a minimal, simple and cross-platform graphics library for PC, Mobile and probably Web (WASM WebGL2 or WebGPU).

Architecture has [Irrlicht](http://irrlicht.sourceforge.net/)'s spirit (simplicity).
API is inspired by [Sokol](https://github.com/floooh/sokol-samples/).

Started from Irrlicht 1.9 trunk branch, revision 5616.

Clarification: Saga3D is not heavily based-on Irrlicht code. Saga3D is written by authors. It uses very little (under 5%) of original Irrlicht code, including some core math, data classes and abstraction. (Note: Irrlicht's math implementation is replaced with GLM, interface of its math barely changes)

## Why another graphics library?

Since Vulkan's release, the quest for leaving OpenGL behind has started for many people, including us. More over, I find most of cross-platform graphics libraries are either simple (and not flexible or cross-platform) or complex (too many supported APIs and dependencies).

Saga3D is designed to have the simplicity in API and cross-platform power of Vulkan and SDL2. We try to avoid as many dependencies as possible and provide ability to extend Saga3D to your own use case.

## Status

Initial work happens on **Linux**, then will be ported to other platforms. I use Ubuntu 18.04.

### See some working [Application Samples](https://gitlab.com/InnerPieceOSS/Saga3D/tree/master/samples).

**Windows** version is working (since Dec 05 2018) with [MSYS2](http://www.msys2.org/).

**Visual Studio** is also working (since Dec 27 2018, only tested with 64-bit configuration). Please use [vcpkg](https://github.com/Microsoft/vcpkg) to install dependencies.

Works on **Intel**'s integrated GPU (since May 11 2019).

Ivy Bridge and Haswell CPUs are reported as compatible with Vulkan and Saga3D on Linux (Mesa drivers). So you don't need to have strong or new hardware to start developing modern 3D graphics.

**MacOS** is working (since Jul 03 2019). Tested on Mojave 10.14.5 with Intel(R) Iris(TM) Graphics 550.

**iOS** is working (since Aug 05 2019). Tested on iPhone 6.

MacOS and iOS port is done by Tuan Anh Phan.

**Android** is working (since November 27 2019). Tested on Nokia 5.

See [milestones](https://gitlab.com/InnerPieceOSS/Saga3D/-/milestones).

## Start using Saga3D

Make sure you have learnt (and probably master) these topics:
- C++14 programming
- OpenGL Shading Language (GLSL) 4.x
- Graphics programming (depends on what you want to achieve with Saga3D)
- Saga3D's coordinate system is right-handed (x left, y up, z points to screen)

## Development

I encourage contributors to follow example-oriented rule.
It's simple as "Always start with an example" :
- Think about what user will see on the screen
- Design engine behaviors
- Make an example in folder `samples`
- Search for references of implementation and visual quality
- Finish writing the sample's code before changing the library
- Be thoughtful when modifying library's interface
- Remember to preserve coding style

And we would like to work with small commit, revertable change and clear commit message.

## Dependencies

Libraries you need to install: `libsdl2-dev, libglm-dev, libvulkan-dev, libassimp-dev`

Tools: Git, Git LFS, CMake 3.13 or later, C++14 compiler.

Other dependencies (build process will add automatically):
- [V-EZ](https://github.com/GPUOpen-LibrariesAndSDKs/V-EZ)
- [stb_image](https://github.com/nothings/stb)

## Building

- Clone the repository
- git lfs pull
- mkdir build
- cd build
- cmake .. -DCMAKE_INSTALL_PREFIX=`_ABSOLUTE_INSTALL_PATH_` <br />
(This command will clone V-EZ or pull latest changes if it's the second CMake call, build type argument you enters is also used to build V-EZ) <br />
Add `-G "MinGW Makefiles"` if building on Windows with MSYS2
- make -j4 install <br/>
Library will be built and installed to your `_ABSOLUTE_INSTALL_PATH_`
- cd `_ABSOLUTE_INSTALL_PATH_/bin` <br />
(You can run some samples here now)
- If you have Vulkan initialization error or application crashes on Windows at startup, try enabling `SAGA_LOAD_VULKAN` CMake option

## Building on Windows with MSYS2

- You'll have to remove Assimp if installed and install Assimp 4.1 [package](http://repo.msys2.org/mingw/x86_64/mingw-w64-x86_64-assimp-4.1.0-3-any.pkg.tar.xz) manually

## Building on Windows with Visual Studio

- Install [vcpkg](https://github.com/Microsoft/vcpkg)
- Install [Vulkan SDK](https://vulkan.lunarg.com/sdk/home)
- Use vcpkg to install: `assimp, sdl2[vulkan], glm, vulkan`
- Add additional parameters to CMake call: `-G "Visual Studio 15 2017 Win64" -DCMAKE_TOOLCHAIN_FILE=_VCPKG_INSTALL_PATH_/scripts/buildsystems/vcpkg.cmake`
- Build
- Execute Visual Studio target INSTALLL (right click on INSTALL -> Debug -> Start new instance)

Pay attention you should specify platform you have been working on while installing packages into *vcpkg*!
The default is x86, so you you have to type **vcpkg install [package] --triplet x64-windows**.

Make sure you set the CMake `VCPKG_TARGET_TRIPLET` variable to **x64-windows** as well.

# Android

Install Android Studio, SDK, NDK

Install CMake and Ninja and have symlinks at /usr/local/bin/

## Build Saga3D
```
export NDK_PATH=/home/manh/android/sdk/ndk-bundle
export CMAKE_TOOLCHAIN=$NDK_PATH/build/cmake/android.toolchain.cmake

cmake .. \
-DCMAKE_TOOLCHAIN_FILE=${CMAKE_TOOLCHAIN} \
-DCMAKE_SYSTEM_NAME=Android -DCMAKE_ANDROID_NDK=${NDK_PATH} \
-DANDROID_FORCE_ARM_BUILD=TRUE -DANDROID_STL=c++_shared -DANDROID_TOOLCHAIN=clang \
-DANDROID_NATIVE_API_LEVEL=29 -DANDROID_ABI=armeabi-v7a \
-DCMAKE_INSTALL_PREFIX=$PWD/install -DCMAKE_BUILD_TYPE=Release
```
## Build Android Dependencies
(Already included in the repo)

### Build assimp
```
export NDK_PATH=/home/manh/android/sdk/ndk-bundle
export CMAKE_TOOLCHAIN=$NDK_PATH/build/cmake/android.toolchain.cmake

cmake .. \
-DCMAKE_TOOLCHAIN_FILE=${CMAKE_TOOLCHAIN} \
-DCMAKE_SYSTEM_NAME=Android -DCMAKE_ANDROID_NDK=${NDK_PATH} \
-DANDROID_FORCE_ARM_BUILD=TRUE -DANDROID_STL=c++_shared -DANDROID_TOOLCHAIN=clang \
-DANDROID_NATIVE_API_LEVEL=27 -DANDROID_ABI=armeabi-v7a \
-DCMAKE_INSTALL_PREFIX=$PWD/install -DCMAKE_BUILD_TYPE=Release \
-DASSIMP_BUILD_TESTS=OFF -DASSIMP_BUILD_ASSIMP_TOOLS=OFF \
-DASSIMP_BUILD_ALL_IMPORTERS_BY_DEFAULT=FALSE \
-DASSIMP_NO_EXPORT=ON \
-DASSIMP_BUILD_OBJ_IMPORTER=ON

make install
```
### Build SDL2
```
ndk-build NDK_PROJECT_PATH=. APP_BUILD_SCRIPT=./Android.mk APP_PLATFORM=android-27
```
