cmake_minimum_required(VERSION 3.13)

project(Saga3D
  VERSION 0.1.0
  DESCRIPTION "Simple and minimal cross-platform library"
)

# Configure CCache if available
find_program(CCACHE_FOUND ccache)
if(CCACHE_FOUND)
  set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ccache)
  set_property(GLOBAL PROPERTY RULE_LAUNCH_LINK ccache)
endif(CCACHE_FOUND)

if (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
  set(CMAKE_INSTALL_PREFIX ${CMAKE_BINARY_DIR}/install)
  message(STATUS "Not providing CMAKE_INSTALL_PREFIX, default will be ${CMAKE_INSTALL_PREFIX}")
endif()

add_compile_options(-fPIC)

option(SAGA_BUILD_SAMPLES "Build samples" ON)
option(SAGA_NO_LOG "Turn off logging" OFF)
option(SAGA_LOAD_VULKAN "Try turning this on if you have Vulkan error on Windows" OFF)
option(SAGA_BUILD_IOS "Build for IOS" OFF)
option(SAGA_BUILD_DYNAMIC_LIB "Build as dynamic library" OFF)

option(SAGA_ASSIMP "Enable Assimp mesh loaders" ON)
option(SAGA_EXTRA_API_MEMORY_INFO "Enable memory info API" OFF)

if (SAGA_ASSIMP)
  add_definitions(-DSAGA_ASSIMP)
endif()

if (SAGA_EXTRA_API_MEMORY_INFO)
  add_definitions(-DSAGA_EXTRA_API_MEMORY_INFO)
endif()

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_PLATFORM_INDEPENDENT_CODE ON)

if (SAGA_BUILD_IOS)
  add_definitions(-DSAGA_BUILD_IOS)
  set(Vulkan_LIBRARY ${VULKAN_SDK}/MoltenVK/iOS/static/libMoltenVK.a)
  set(Vulkan_INCLUDE_DIR ${VULKAN_SDK}/MoltenVK/include)
endif()

find_package(Vulkan REQUIRED)

if (SAGA_NO_LOG)
  add_definitions(-DSAGA_NO_LOG)
endif()

if (SAGA_LOAD_VULKAN)
  add_definitions(-DSAGA_LOAD_VULKAN)
endif()

add_subdirectory(external)
add_subdirectory(library)

if (SAGA_BUILD_SAMPLES)
  add_subdirectory(samples)

  install(DIRECTORY media
    DESTINATION "${CMAKE_INSTALL_PREFIX}"
  )
endif()